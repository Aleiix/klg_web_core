export const load = (elementName: string, props: {[p: string]: any} = {}, outputs: {[p: string]: () => void} = {}) => {
    const element = document.createElement(elementName);
    Object.keys(props).forEach((key) => {
        element[key] = props[key];
    });
    Object.keys(outputs).forEach((event) => {
        element.addEventListener(event, outputs[event]);
    });
    window.onbeforeunload = function (event) {
        Object.keys(outputs).forEach((event) => {
            element.removeEventListener(event, outputs[event]);
        });
    }
    return element;
}

