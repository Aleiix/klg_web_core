const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const path = require('path');

module.exports = {
    entry: ['./src/scripts/element-loader/element-loader.ts'],
    devtool: 'inline-source-map',
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: [
                    /node_modules/
                ]
            },
        ],
    },
    resolve: {
        extensions: [ '.tsx', '.ts', '.js' ],
    },
    plugins: [
        new CleanWebpackPlugin(),
        new CopyWebpackPlugin({
            patterns: [
                {
                    from: '**/*.scss',
                    to: 'styles',
                    context: 'src/styles'
                }
            ]
        })
    ],
    output: {
        filename: 'bundle.js',
        library: 'elementLoader',
        path: path.resolve(__dirname, 'dist'),
    },
};
